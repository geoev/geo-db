from setuptools import setup, find_packages

with open('requirements.txt') as reqs:
    deps = reqs.readlines()


setup(
    name="geo-db",
    version='0.1.0',
    packages=find_packages(),
    scripts=[],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=deps,

    # metadata for upload to PyPI
    author="Victor Cassen",
    author_email="vmc.swdev@gmail.com",
    description="A variety of general utilities",
    license="MIT",
    keywords="",
    url="https://gitlab.com/geoev/geo-db",

    # project_urls={
    #     "Bug Tracker": "https://bugs.example.com/HelloWorld/",
    #     "Documentation": "https://docs.example.com/HelloWorld/",
    #     "Source Code": "https://code.example.com/HelloWorld/",
    # }

    # could also include long_description, download_url, classifiers, etc.
)
