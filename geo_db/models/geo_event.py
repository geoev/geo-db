'''
Geoevent models
'''
from typing import List, Optional
import json
from random import random
from pydantic import BaseModel, validator, ValidationError
from uuid import uuid4
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
import arrow

class GeoEvent(BaseModel):
    lat: float
    lon: float
    src: str
    evt_type: str
    key: Optional[str]
    value: Optional[str]
    itimestamp: Optional[int]
    userid: Optional[str]

    @validator('value')
    def value_must_be_json(cls, value):
        try:
            json.loads(value)
            return value        # has to be value and not anything else
        except json.decoder.JSONDecodeError as e:
            raise ValidationError(F"GeoEvent.value does not parse as JSON ({e})")

    @classmethod
    def random_geoevent(cls, **kwargs):
        evt = cls(
            userid=str(uuid4())[:32],
            itimestamp=arrow.utcnow().int_timestamp,
            src=kwargs.get('src', 'geo_cli'),
            evt_type='random',
            lat=random()*180.0 - 90.0,
            lon=random()*360.0 - 180.0)
        if 'key' in kwargs:
            evt['key'] = key
        if 'value' in kwargs:
            evt['value'] = value
        return evt

class CreateGeoEvent(GeoEvent):
    pass

class CreateGeoEvents(BaseModel):
    events: List[GeoEvent]

# SQLAlchemy:
metadata = sa.MetaData()
geo_event_table = sa.Table(
    "geo_event",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("lat", sa.Float, nullable=False),
    sa.Column("lon", sa.Float, nullable=False),
    sa.Column("src", sa.String, nullable=False, default='unknown'),
    sa.Column("evt_type", sa.String, nullable=False),
    sa.Column("key", sa.String, nullable=True),
    sa.Column("value", JSONB, nullable=True),
    sa.Column("itimestamp", sa.Integer),
    sa.Column("userid", sa.String, nullable=True)
)
