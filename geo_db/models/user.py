'''
Geo User models
'''
from pydantic import BaseModel, validator, ValidationError
from uuid import uuid4
from random import randint

class User(BaseModel):
    userid: str
    firstname: str
    lastname: str

    @classmethod
    def random_user(cls, **kwargs):
        return User(
            userid=str(uuid4()),
            firstname=''.join(chr(randint(ord('a'), ord('z'))) for _ in range(6)),
            lastname=''.join(chr(randint(ord('a'), ord('z'))) for _ in range(6)),
        )
        
