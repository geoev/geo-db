"""add key-value

Revision ID: 0f0bc2b9f3ff
Revises: c4c1d3f3c055
Create Date: 2022-12-26 21:09:04.925582

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.types import String
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic.
revision = '0f0bc2b9f3ff'
down_revision = 'c4c1d3f3c055'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('geo_event', sa.Column('key', String(32)))
    op.add_column('geo_event', sa.Column('value', JSONB))
    op.create_index('gen_event_key_idx', 'geo_event', ['key'])


def downgrade() -> None:
    op.drop_column('geo_event', 'value')
    op.drop_index('gen_event_key_idx', 'geo_event')
