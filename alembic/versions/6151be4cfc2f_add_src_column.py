"""add src column

Revision ID: 6151be4cfc2f
Revises: 547a522f65d9
Create Date: 2023-01-14 21:26:25.553061

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6151be4cfc2f'
down_revision = '547a522f65d9'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('geo_event', sa.Column('src', sa.Text, nullable=False, server_default='unknown'))
    op.create_index('gen_event_src_idx', 'geo_event', ['src'])


def downgrade() -> None:
    op.drop_column('geo_event', 'src')
    op.drop_index('gen_event_src_idx', 'geo_event')
