"""description to ev_type

Revision ID: 2f52348cac3d
Revises: 0f0bc2b9f3ff
Create Date: 2023-01-02 19:39:37.842103

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2f52348cac3d'
down_revision = '0f0bc2b9f3ff'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('geo_event', 'description', nullable=False, new_column_name='ev_type')
    op.create_index('gen_event_type_idx', 'geo_event', ['ev_type'])
    

def downgrade() -> None:
    op.alter_column('geo_event', 'ev_type', nullable=False, new_column_name='description')

