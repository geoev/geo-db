"""create geo_event table

Revision ID: c4c1d3f3c055
Revises: 
Create Date: 2022-12-20 22:03:40.559726

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c4c1d3f3c055'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
   op.create_table(
        'geo_event',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('lat', sa.Float(50), nullable=False),
        sa.Column('lon', sa.Float(50), nullable=False),
        sa.Column('description', sa.Unicode(200)),
    )


def downgrade() -> None:
    op.drop_table('geo_event')
