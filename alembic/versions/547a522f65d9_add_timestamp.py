"""add timestamp

Revision ID: 547a522f65d9
Revises: 2f52348cac3d
Create Date: 2023-01-02 19:50:41.737957

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '547a522f65d9'
down_revision = '2f52348cac3d'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('geo_event', sa.Column('timestamp', sa.DateTime(timezone=True), server_default=sa.text('now()')))


def downgrade() -> None:
    op.drop_column('geo_event', 'timestamp')
