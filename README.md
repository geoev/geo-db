# geo-db

[Google console](https://console.cloud.google.com/sql/instances/geo-db/overview?project=freetier-219419)

# Todo:
- enable private ips (probably need to add some permissions)
[Configure an instance to use private IP](https://cloud.google.com/sql/docs/mysql/configure-private-ip?_ga=2.264601671.-916740845.1671603570#configure_an_instance_to_use_private_ip "Google Docs")

- add deploy/*/env-vars.sops.yml
- implement as k8 job
(or: deploy to VM)
implement lat/lon as postgis


