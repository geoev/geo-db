{
	"data": "ENC[AES256_GCM,data:U/b3NNhhQTsi2bvG9DAelp82XcqJfByPFvBsuguSyqrYtjNt+wU1q51C2rV1ryhVzDRy9RAoj0LR1UFTEsq+MU7DGZcIHK4d3Z0IJhaEABfWNvMbohns70nGzcw/f0fHoCD4+0rbYWeEudsUbPW11oJa0gICscBNq5Y77PdqYnny29mbBerYi84tlImMQYvnwtf7/kKzGKY=,iv:KVZ8RcrkY6KtL8YARr+oB5xpoZClZaKKOzC3yOI/pDI=,tag:WQ6AP7qh/ezYltEArZuAqA==,type:str]",
	"sops": {
		"kms": null,
		"gcp_kms": [
			{
				"resource_id": "projects/freetier-219419/locations/us-west1/keyRings/geo-event/cryptoKeys/geo-key1",
				"created_at": "2023-01-15T23:52:24Z",
				"enc": "CiQARCazYKXk0yPYr29N7XBnnNda1y+Sr3LLEk9EzCqsuoMyOeoSSQBsz71CxtI6Z3WLwHb5fCOlHxv6/Tx7nUt8+MP7bmfsqIuYUqIyvlPUuwvJKO2Q+jptVnTyfKmTQAaNxAHDuGLDcs2MMK0meNA="
			}
		],
		"lastmodified": "2023-01-15T23:52:24Z",
		"mac": "ENC[AES256_GCM,data:IEwejJKPeTM5ShehH75LVX388/mkPk9d7fstMHUVelDbgT9RRXpxhphkeGVAaAkKGpz95dgITfLUkU9d/TfmHBxbsWUgoN7X4WfzHNenhBLaw9boyf19Dc6tnNgtVYH0E80zWSSgXKbgLKvrkYitomgzpdSXEnMwlbH01wyiHL4=,iv:BxSw/9n+GuoI9oVEsX1m27L5GP4UzUguFt1g6Bz1U+0=,tag:JXgyt/hCkDodr/5v69Kdiw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.0.5"
	}
}